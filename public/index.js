import {TransactionUnspentOutput} from './cardano_serialization_lib.js';

// Vars
var enabled = false;
var policyArt = "8a3e9f98aee1972b2841fd43802266d595ea8d0f823bbcdd3c44e7de";
var policyEAT = "979e11bcdbe12ad8006583b1b2622f1c94a6b75d66b9151cf87d5550"
var addr_link = ['<a href="' + "https://pool.pm/",
                 '" target="_blank">Show your ', ' on pool.pm</a>']

var dup_ids = [569, 4, 7, 10, 20, 23, 24, 31, 33, 34, 37, 38, 40, 43, 44, 45, 46, 59, 69, 76, 77, 79, 87, 88, 96, 97, 109, 114, 116, 118, 121, 123, 131, 144, 146, 150, 157, 158, 166, 168, 172, 173, 177, 191, 199, 202, 203, 205, 217, 219, 221, 223, 224, 227, 228, 229, 241, 242, 244, 248, 250, 256, 258, 260, 263, 270, 275, 277, 279, 285, 291, 292, 293, 295, 296, 300, 307, 313, 316, 323, 341, 346, 347, 351, 354, 356, 358, 359, 360, 362, 369, 371, 374, 380, 382, 385, 393, 395, 396, 404, 406, 409, 410, 412, 426, 434, 437, 439, 443, 449, 464, 472, 473, 475, 514, 515, 516, 517, 520, 524, 530, 545, 547, 548, 556, 559, 560, 562, 567, 582, 589, 592, 593, 594, 600, 601, 602, 611, 613, 615, 617, 620, 623, 624, 626, 636, 640, 644, 652, 661, 664, 668, 670, 678, 680, 686, 687, 689, 690, 700, 707, 714, 718, 719, 722, 726, 730, 731, 734, 740, 747, 754, 755, 756, 762, 765, 772, 774, 776, 784, 786, 792, 806, 809, 810, 811, 813, 815, 817, 821, 826, 838, 841, 842, 844, 845, 848, 850, 856, 866, 875, 880, 882, 883, 886, 895, 899, 902, 913, 917, 923, 938, 941, 943, 944, 945, 947, 949, 952, 954, 960, 963, 966, 969, 970, 972, 988, 989, 990, 998, 1010, 1011, 1012, 1019, 1020, 1030, 1031, 1032, 1034, 1040, 1050, 1052, 1053, 1056, 1059, 1061, 1068, 1073, 1079, 1080, 1082, 1094, 1097, 1101, 1109, 1113, 1115, 1118, 1119, 1120, 1124, 1130, 1141, 1144, 1152, 1153, 1162, 1163, 1180, 1193, 1196, 1200, 1201, 1206, 1214, 1217, 1222, 1224, 1226, 1232, 1237, 1240, 1241, 1242, 1248, 1250, 1270, 1276, 1277, 1279, 1281, 1285, 1289, 1294, 1298, 1303, 1306, 1307, 1318, 1330, 1331, 1344, 1345, 1346, 1347, 1350, 1351, 1356, 1358, 1363, 1365, 1369, 1370, 1371, 1372, 1376, 1380, 1383, 1392, 1393, 1394, 1399, 1402, 1406, 1417, 1419, 1420, 1423, 1434, 1438, 1440, 1443, 1456, 1460, 1462, 1465, 1473, 1476, 1478, 1486, 1488, 1492, 1493, 1494, 1496, 1504, 1506, 1508, 1509, 1522, 1529, 1532, 1548, 1550, 1556, 1557, 1560, 1568, 1574, 1576, 1580, 1582, 1583, 1591, 1595, 1598, 1600, 1605, 1611, 1617, 1618, 1622, 1627, 1629, 1633, 1634, 1635, 1640, 1641, 1658, 1659, 1661, 1662, 1663, 1667, 1691, 1692, 1698, 1699, 1700, 1704, 1713, 1716, 1718, 1719, 1721, 1724, 1728, 1731, 1732, 1733, 1745, 1750, 1751, 1755, 1757, 1759, 1767, 1780, 1781, 1800, 1808, 1811, 1813, 1816, 1817, 1824, 1827, 1835, 1840, 1843, 1844, 1848, 1849, 1850, 1863, 1866, 1867, 1868, 1871, 1877, 1879, 1882, 1883, 1889, 1891, 1903, 1904, 1905, 1907, 1909, 1914, 1915, 1918, 1921, 1925, 1927, 1933, 1937, 1938, 1939, 1941, 1942, 1952, 1957, 1959, 1966, 1969, 1974, 1975, 1979, 1983, 1984, 1988, 1989, 1995, 1996, 1998]

//dup_ids = dup_ids + [1754]


var tables = []
tables[0] = document.getElementById("insomniumArtTab");
tables[1] = document.getElementById("insomniumEATTab");
tables[2] = document.getElementById("insomniumBurnTab");
var burnText = document.getElementById("insomniumBurnText");
var burnDisclaimerText = document.getElementById("insomniumBurnDisclaimer");

// Vars

// Functions
function toHexString(byteArray) {

  return Array.from(byteArray, function(byte) {
    return ('0' + (byte & 0xFF).toString(16)).slice(-2);
  }).join('')
  
}

function parseHexString(str) {

    var result = [];
    while (str.length >= 2) {
        result.push(parseInt(str.substring(0, 2), 16));
        str = str.substring(2, str.length);
    }
    return result;
    
}

function hex2a(hexx) {

    var hex = hexx.toString();//force conversion
    var str = '';
    for (var i = 0; i < hex.length; i += 2)
        str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
    return str;
    
}

function resetButton() {

  var buttonReference = document.getElementById('connectBtn');
  buttonReference.innerHTML="Connect"
  buttonReference.disabled = false;
  enabled = false
  
}

async function cnt(){

  cardano.enable().then(response => {
    // console.log(response);
  }).catch(e => {
    console.log(e);
    resetButton()
  });
  
}

function addCell(tr, text) {

 var td = tr.insertCell();
 td.bored="1 px"
 td.textContent = text;
 return td;
 
}

function addButtonCell(tr, link, text) {

 var td = tr.insertCell();
 td.bored="1 px"
 td.innerHTML = '<button class="btn btn-info" type="button" onClick="window.open(\'' + link + '\')" >'+ text + '</button>';
 return td;
 
}

function burnVisibility() {
  tables[2] = document.getElementById("insomniumBurnTab");
  tables[2].style.visibility='visible';
  burnText.style.visibility='visible';
  burnDisclaimerText.style.visibility='visible';
}

function process_ass_key(tx, assets, rows, key_raw, money, i, j) {

  var asset_key=tx.output().amount().multiasset().get(key_raw).keys().get(j)
  var key=tx.output().amount().multiasset().keys().get(i).to_bytes()
  var ast_nm = toHexString(tx.output().amount().multiasset().get(key_raw).keys().get(j).name())
  var asset=hex2a(toHexString(tx.output().amount().multiasset().get(key_raw).keys().get(j).name()))
  var amount=tx.output().amount().multiasset().get(key_raw).get(asset_key).to_str()
  
  function checkPolicy(policy, rows, id) {
    if (toHexString(key) == policy) {
      assets=assets+asset
      
      tables[id].style.visibility='visible';
      
      addCell(rows[id], asset);
      addButtonCell(rows[id], "https://pool.pm/"+policy+"."+asset, "pool.pm");
      addButtonCell(rows[id], "https://cardanoscan.io/token/"+policy+ast_nm+"?tab=minttransactions", "cardanoscan.io");  
    }
  }
  
  checkPolicy(policyArt, rows, 0)
  checkPolicy(policyEAT, rows, 1)
  if (dup_ids.includes(parseInt(asset.substring(12)))) {
    checkPolicy(policyArt, rows, 2)
    burnVisibility()
  }
  money=money+" +"+amount+" "+toHexString(key)+"."+asset
  
  return money, assets
}

function process_assets(tx, assets, money, tables) {
  try {
  
    var rows = [];
    var multi_ass = tx.output().amount().multiasset()
    
    for (let i = 0; i < multi_ass.len(); i++) {
    
      var multi_ass_k = multi_ass.keys()
      var key=multi_ass_k.get(i).to_bytes()
      var key_raw=multi_ass_k.get(i)
      
      for (let j=0;j<multi_ass.get(key_raw).len();j++){
      
        rows[0] = tables[0].insertRow();
        rows[1] = tables[1].insertRow();
        rows[2] = tables[2].insertRow();
        
        money, assets = process_ass_key(tx, assets, rows, key_raw, money, i, j)
      }
    }
  }catch(error) {
    console.log("no assets" + error)
  }
  
  return assets, money
}

function processTxs(utxos, balance=0){

  utxos.forEach((tx) => {
    try {
      
      var txid=toHexString(tx.input().transaction_id().to_bytes())+" #"+tx.input().index()
      var ada=tx.output().amount().coin().to_str()
      var assets=""
      var money=toHexString(tx.input().transaction_id().to_bytes())+" #"+tx.input().index()+"  "+tx.output().amount().coin().to_str()+" adalace"
      
      assets, money = process_assets(tx, assets, money, tables)
      balance=balance+parseInt(tx.output().amount().coin().to_str())
      
    }catch(e){
      console.log(e);
    }
  });
  
  return balance;
}

async function main(){

  let utxos = (await window.cardano.getUtxos()).map((utxo) =>
     TransactionUnspentOutput.from_bytes(
       Buffer.Buffer.from(utxo, "hex")
     )
  )
  
  var balance = processTxs(utxos)

  balance=balance/1000000
  var address=utxos[0].output().address().to_bech32()
  
  document.getElementById("artAddr").innerHTML = addr_link[0] + address + "/" + policyArt.substring(0,7) + addr_link[1] + "Insomnium Art assets" + addr_link[2]
  document.getElementById("eatAddr").innerHTML = addr_link[0] + address + "/" + policyEAT.substring(0,7) + addr_link[1] + "Insomnium EAT assets" + addr_link[2]
  document.getElementById("connectBtn").innerHTML=address.substring(0, 24)+"..."
}

// Functions
// Logic

// Button mechanics
var buttonReference = document.getElementById('connectBtn');
buttonReference.onclick = function() {
  console.log('button clicked');
  if (enabled) {
    buttonReference.disabled = true;
    main()
  }
  if (cnt()) {
    buttonReference.innerHTML="Read Wallet"
    enabled = true;
  }
}
